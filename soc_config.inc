ISA=RV64IMAC
MUL=fpga
SYNTH=SIM
MAINMEM=BRAM
VERBOSITY=2
BOOTROM=enable
USERTRAPS=False
USER=True
COREFABRIC=AXI4
MULSTAGES=8
DIVSTAGES=64
FPGA=xc7a100tcsg324-1
SYNTHTOP=mkriscv
COUNTERS=0
PADDR=32
RESETPC=4096

# Verilator options
COVERAGE=none
TRACE=disable
THREADS=1

# DebugOptions
RTLDUMP=True

# Instruction cache
ICACHE=enable
ISETS=64
IWORDS=4
IBLOCKS=8
IWAYS=4
IFBSIZE=4
IREPL=PLRU
IRESET=1

#Pipe depths
PIPE1=6
